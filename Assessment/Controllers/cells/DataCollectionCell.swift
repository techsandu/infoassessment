//
//  DataCollectionCell.swift
//  Assessment
//
//  Created by Sandeep on 16/01/19.
//  Copyright © 2019 Sandeep. All rights reserved.
//

import UIKit

class DataCollectionCell: UICollectionViewCell {
 
     var titleLabel: UILabel = {
        let controller = UILabel()
        controller.translatesAutoresizingMaskIntoConstraints = false
        controller.textAlignment = .left
        return controller
    }()
    var descriptionLabel:UILabel = {
        let controller = UILabel()
        controller.numberOfLines = 0
        controller.font = UIFont.systemFont(ofSize: 16, weight: UIFont.Weight.regular)
        //controller.backgroundColor = .red
        controller.translatesAutoresizingMaskIntoConstraints = false
        controller.textAlignment = .left
        return controller
    }()
    
    var itemImage:UIImageView = {
        let controller = UIImageView()
        controller.translatesAutoresizingMaskIntoConstraints = false
        //controller.image = UIImage(named: "placeHolder")
        return controller
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentView.addSubview(titleLabel)
        self.addSubview(descriptionLabel)
        self.contentView.addSubview(itemImage)
        self.contentView.backgroundColor = .white
       
    }
    override func layoutSubviews() {
        actiVateContrain()
        self.layoutIfNeeded()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        fatalError("Interface Builder is not supported!")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        actiVateContrain()
        self.layoutIfNeeded()
        self.descriptionLabel.text = nil
    }
    
    func actiVateContrain(){
        let constrains = [
  
            
            itemImage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 10),
            itemImage.widthAnchor.constraint(equalToConstant: 60),
            itemImage.topAnchor.constraint(equalTo: self.topAnchor, constant: 10),
            itemImage.heightAnchor.constraint(equalToConstant: 60),
            
            
            titleLabel.topAnchor.constraint(equalTo: self.topAnchor,constant:CGFloat(10)),
            titleLabel.leadingAnchor.constraint(equalTo: self.itemImage.trailingAnchor,constant:10),
            titleLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            titleLabel.heightAnchor.constraint(equalToConstant: 18),
            
            descriptionLabel.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: CGFloat(8)),
            descriptionLabel.leadingAnchor.constraint(equalTo: self.titleLabel.leadingAnchor),
            descriptionLabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: CGFloat(-10)),
            descriptionLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: CGFloat(-10)),
          
            ]
        NSLayoutConstraint.activate(constrains)
    }
 
}
