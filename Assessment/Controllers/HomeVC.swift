//
//  HomeVC.swift
//  Assessment
//
//  Created by Sandeep on 16/01/19.
//  Copyright © 2019 Sandeep. All rights reserved.
//

import UIKit
import SDWebImage

class HomeVC: UIViewController {
    lazy var opQueue = OperationQueue()
    lazy var overlay = LoadingOverlay()
    var tableData = [ResponseModel]()
     var ImageCache = [String:UIImage]()
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(HomeVC.handleRefresh(_:)),
                                 for: UIControl.Event.valueChanged)
        refreshControl.tintColor = .white
        
        return refreshControl
    }()
    let homeView : UIView = {
       let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    lazy var dataCollection : UICollectionView = {
        let controller = UICollectionView(frame: CGRect.zero, collectionViewLayout: UICollectionViewFlowLayout.init())
        controller.translatesAutoresizingMaskIntoConstraints = false
        controller.contentInset = UIEdgeInsets(top: 23, left: 16, bottom: 10, right: 16)
        controller.backgroundColor = .gray
        return controller
    }()
    let layout:UICollectionViewFlowLayout = UICollectionViewFlowLayout.init()
    
    override func viewDidLoad() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(fetchData))
        super.viewDidLoad()
       setupDataCollection()
      self.dataCollection.addSubview(self.refreshControl)
       fetchData()
        // Do any additional setup after loading the view.
    }
    
    

    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
   
        fetchData()
        refreshControl.endRefreshing()
    }
    func setupDataCollection(){
        
        layout.scrollDirection = UICollectionView.ScrollDirection.vertical
        dataCollection.setCollectionViewLayout(layout, animated: true)
        dataCollection.delegate = self
        dataCollection.dataSource = self
        
        self.homeView.addSubview(dataCollection)
        dataCollection.register(DataCollectionCell.self, forCellWithReuseIdentifier: "cell")
    }
    func AddsubViews(){
        homeView.frame = self.view.frame
        dataCollection.frame = self.view.frame
        self.view.addSubview(homeView)
    }

    override func viewDidLayoutSubviews() {
         AddsubViews()
    }
    func addCons(){
        
    }
    @objc func fetchData(){
        tableData.removeAll()
        let url = Constatnts.APIs.baseURL+Constatnts.APIs.fact
        overlay.showOverlay(view: self.view, title: "fetching Data")
        ServiceHandler().webServiceCallGet(url: url) { (response) in
            DispatchQueue.main.async {
                self.overlay.hideOverlayView()
               
                if let title = response.value(forKey: "title") as? String{
                     self.title = title
                }
                if let rowValue = response.value(forKey: "rows") as? NSArray{
                    for items in rowValue {
                        if let item = items as? NSDictionary{
                            self.tableData.append(ResponseModel().getData(data: item))
                        }
                    }
                    self.dataCollection.reloadData()
                }
            }
        }
        
    }
    private func estimateFrameForText(text: String) -> CGRect {
        //we make the height arbitrarily large so we don't undershoot height in calculation
        let height: CGFloat = 650
        
        let size = CGSize(width:(dataCollection.frame.width - 90) , height: height)
        
        
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20, weight: UIFont.Weight.regular)]
        
  
        return NSString(string: text).boundingRect(with: size, options: options, attributes: attributes, context: nil)
     
       
    }

}
extension HomeVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tableData.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! DataCollectionCell
        cell.backgroundColor = .white
     
        
        cell.titleLabel.text = tableData[indexPath.row].title
        cell.descriptionLabel.text = ""
        cell.descriptionLabel.text = tableData[indexPath.row].description
        cell.itemImage.image = UIImage(named: "placeHolder")
        if let imageUrl = tableData[indexPath.row].imageUrl{
            if let cachedImage = ImageCache[imageUrl]{
                cell.itemImage.image = cachedImage
            }else{
                opQueue.addOperation {
                    let downloadedImage = ServiceHandler.downloadImageWithURL(urlString: imageUrl)
                    OperationQueue.main.addOperation({
                        cell.itemImage.image = downloadedImage
                        self.ImageCache[imageUrl] = downloadedImage
                    })
                }
            }
        }
      //  cell.frame.height =
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         let itemSize = (collectionView.frame.width - (collectionView.contentInset.left + collectionView.contentInset.right + 10))
         let itemheight = estimateFrameForText(text:tableData[indexPath.item].description )
        var height:CGFloat = 80
        if itemheight.height < 80{
            height = itemheight.height + ((80 - itemheight.height) + 20)
        }else{
            height = itemheight.height + 20
        }
        
        
        return CGSize(width: itemSize, height: height)
    }
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
      dataCollection.invalidateIntrinsicContentSize()
    }
}
extension String {
    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.height)
    }
    
    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return ceil(boundingBox.width)
    }
}
