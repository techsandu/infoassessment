//
//  ServiceLayer.swift
//  Assessment
//
//  Created by Sandeep on 16/01/19.
//  Copyright © 2019 Sandeep. All rights reserved.
//

import Foundation
import Alamofire

class ServiceHandler:NSObject{
 
        class func downloadImageWithURL(urlString:String) -> UIImage! {
            var downloadedImage = UIImage(named: "placeHolder")
            print("items image coming \(urlString)")
            if let url  = URL(string: urlString){
                    do{
                        let imageData = try Data(contentsOf:url)
                        downloadedImage = UIImage(data:imageData)
                        return downloadedImage
                    }catch{
                        return downloadedImage
                    }
            }else{
                return downloadedImage
            }
        }
    
    func webServiceCallGet(url:String,completion:@escaping(NSDictionary)->()) {
        
        
        if isInternetAvailable(){
            
            let ulr =  NSURL(string:url)
            let headers = ["Content-Type": "application/x-www-form-urlencoded"]
            Alamofire.request(ulr! as URL, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)
                .responseString { response in
                    //to get JSON return value
                    if response.result.error != nil{
                        let jsonDict:NSDictionary = [
                            "status":0,
                            "message":response.result.error?.localizedDescription
                        ]
                        completion(jsonDict)
                    }else{
                        
                        if let stringArray = response.result.value,let StringData = stringArray.data(using: .utf8){
                            do{
                                if let jsonData = try JSONSerialization.jsonObject(with: StringData, options: .allowFragments) as? NSDictionary{
                                   
                                    completion(jsonData)
                                }else{
                                    let jsonDict:NSDictionary = [
                                        "status":0,
                                        "message":"something went wrong"
                                    ]
                                    completion(jsonDict)
                                }
                            }catch{
                                let jsonDict:NSDictionary = [
                                    "status":0,
                                    "message":"something went wrong"
                                ]
                                completion(jsonDict)
                            }
                        }
                    }
                    
            }
        }else{
            let jsonDict:NSDictionary = [
                "status":0,
                "message":"Please check your Internet connection"
            ]
            completion(jsonDict)
            
        }
        
        
    }
}

