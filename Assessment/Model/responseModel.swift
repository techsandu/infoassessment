//
//  responseModel.swift
//  Assessment
//
//  Created by Sandeep on 16/01/19.
//  Copyright © 2019 Sandeep. All rights reserved.
//

import Foundation

class ResponseModel{
    var description = ""
    var imageUrl:String?
    var title:String?
    
     func getData(data:NSDictionary)->ResponseModel{
        if let description = data.value(forKey: "description") as? String{
            self.description = description
        }
        if let imageUrl = data.value(forKey: "imageHref") as? String{
            self.imageUrl = imageUrl
        }
        if let title = data.value(forKey: "title") as? String{
            self.title = title
        }
        return self
    }
}
