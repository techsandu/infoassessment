//
//  Overlay.swift
//  Assessment
//
//  Created by Sandeep on 16/01/19.
//  Copyright © 2019 Sandeep. All rights reserved.
//

import Foundation
import UIKit

public class LoadingOverlay {
    
    var overlayView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    var loadingLabel = UILabel()
    
    class var shared: LoadingOverlay {
        struct Static {
            static let instance: LoadingOverlay = LoadingOverlay()
        }
        return Static.instance
    }
    
    public func showOverlay(view: UIView,title:String) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        overlayView.frame = (appDelegate.window?.bounds)!  //view.frame
        overlayView.center = (appDelegate.window?.center)! //view.center
        overlayView.backgroundColor = .black
        overlayView.alpha = 0.8
        overlayView.clipsToBounds = true
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.style = .whiteLarge
        activityIndicator.center = CGPoint(x: overlayView.bounds.width / 2, y: overlayView.bounds.height / 2 - 50)
        
        loadingLabel.frame = CGRect(x: 0, y: activityIndicator.frame.origin.y + 45, width: view.frame.size.width, height: 30)
        loadingLabel.text = title
        loadingLabel.textAlignment = .center
        loadingLabel.textColor = .white
        
        overlayView.addSubview(activityIndicator)
        overlayView.addSubview(loadingLabel)
        //        view.addSubview(overlayView)
        appDelegate.window?.addSubview(overlayView)
        
        activityIndicator.startAnimating()
    }
    
    public func hideOverlayView() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.overlayView.removeFromSuperview()
        }
        
    }
    
}
